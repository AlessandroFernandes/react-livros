import React, { Component } from 'react';
import './assets/css/pure.css';
import './assets/css/side-menu.css';
//import $ from 'jquery';
import { Link } from 'react-router-dom';
class App extends Component {
	render() {
		return (
			<div id="layout">
				<a href="#menu" id="menuLink" className="menu-link">
					<span />
				</a>

				<div id="menu">
					<div className="pure-menu">
						<a className="pure-menu-heading" href="#">
							Company
						</a>

						<ul className="pure-menu-list">
							<li className="pure-menu-item">
								<Link to="/" className="pure-menu-link">
									Home
								</Link>
							</li>
							<li className="pure-menu-item">
								<Link to="/autores" className="pure-menu-link">
									Autor
								</Link>
							</li>

							<li className="pure-menu-item menu-item-divided pure-menu-selected">
								<a href="/livros" className="pure-menu-link">
									Livros
								</a>
							</li>
						</ul>
					</div>
				</div>

				<div id="main">{this.props.children}</div>
			</div>
		);
	}
}

export default App;
