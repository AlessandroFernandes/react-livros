import React, { Component } from 'react';
import axios from 'axios';
import InputCustomizado from '../common/NovoInput';
import PubSub from 'pubsub-js';
import Erros from '../common/Erros';
import $ from 'jquery';

class TabelasAutores extends Component {
	render() {
		return (
			<div>
				<table className="pure-table">
					<thead>
						<tr>
							<th>Nome</th>
							<th>email</th>
						</tr>
					</thead>
					<tbody>
						{this.props.lista.map(autor => (
							<tr key={autor.id}>
								<td>{autor.nome}</td>
								<td>{autor.email}</td>
							</tr>
						))}
					</tbody>
				</table>
			</div>
		);
	}
}

class AdicionarAutores extends Component {
	constructor() {
		super();
		this.state = { nome: '', email: '', senha: '' };
		this.setNome = this.setNome.bind(this);
		this.setEmail = this.setEmail.bind(this);
		this.setSenha = this.setSenha.bind(this);
	}

	enviarForm(event) {
		event.preventDefault();
		$.ajax({
			url: `http://cdc-react.herokuapp.com/api/autores`,
			type: 'POST',
			contentType: 'application/json',
			dataType: 'json',
			data: JSON.stringify({
				nome: this.state.nome,
				email: this.state.email,
				senha: this.state.senha,
			}),
			success: function(resposta) {
				PubSub.publish('autores-lista', resposta);
				console.log('Enviado com sucesso!');
				this.setState({ nome: '', email: '', senha: '' });
			},
			error: function(error) {
				if (error.status === 400) {
					new Erros().publicaErros(error.responseJSON);
				}
			},
			beforeSend: () => PubSub.publish('limpar-erros', {}),
		});
		/*
		axios
			.post(`http://cdc-react.herokuapp.com/api/autores`, {
				nome: this.state.nome,
				email: this.state.email,
				senha: this.state.senha,
			})
			.then(
				resposta => PubSub.publish('autores-lista', resposta),
				console.log('Enviado com sucesso!'),
			);*/
	}

	setNome(event) {
		this.setState({ nome: event.target.value });
	}

	setEmail(event) {
		this.setState({ email: event.target.value });
	}

	setSenha(event) {
		this.setState({ senha: event.target.value });
	}

	render() {
		return (
			<form
				className="pure-form pure-form-aligned"
				onSubmit={this.enviarForm.bind(this)}
				method="post"
			>
				<InputCustomizado
					id="nome"
					type="text"
					name="nome"
					value={this.state.nome}
					onChange={this.setNome}
					label="Nome"
				/>
				<InputCustomizado
					id="email"
					type="email"
					name="email"
					value={this.state.email}
					onChange={this.setEmail}
					label="Email"
				/>
				<InputCustomizado
					id="senha"
					type="password"
					name="senha"
					value={this.state.senha}
					onChange={this.setSenha}
					label="Senha"
				/>
				<div className="pure-control-group">
					<label />
					<button
						type="submit"
						className="pure-button pure-button-primary"
					>
						Gravar
					</button>
				</div>
			</form>
		);
	}
}

export default class AutoresBox extends Component {
	constructor() {
		super();
		this.state = { lista: [] };
	}
	componentDidMount() {
		axios
			.get(`http://cdc-react.herokuapp.com/api/autores`)
			.then(resposta => this.setState({ lista: resposta.data }))
			.catch(erro => console.log(erro));

		async function atualiza() {
			await PubSub.subscribe('autores-lista', (index, resposta) =>
				this.setState({ lista: resposta }),
			);
		}
	}

	render() {
		return (
			<div>
				<div className="header">
					<h1>Cadastro de Autores</h1>
				</div>
				<div className="content" id="content">
					<AdicionarAutores />
					<TabelasAutores lista={this.state.lista} />
				</div>
			</div>
		);
	}
}
