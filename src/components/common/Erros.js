import PubSub from 'pubsub-js';

export default class Erros {
	publicaErros(error) {
		for (var x = 0; x < error.errors.length; x++) {
			let erros = error.errors[x];
			console.log(erros);
			PubSub.publish('erro-de-validacao', erros);
		}
	}
}
