import React, { Component } from 'react';
import PubSub from 'pubsub-js';

export default class NovoInput extends Component {
	constructor() {
		super();
		this.state = {
			erroMsg: '',
		};
	}

	componentDidMount() {
		PubSub.subscribe('erro-de-validacao', (index, error) => {
			if (error.field === this.props.name) {
				this.setState({ erroMsg: error.defaultMessage });
			}
		});
		PubSub.subscribe('limpar-erros', index => {
			this.setState({ erroMsg: '' });
		});
	}
	render() {
		return (
			<div className="pure-control-group">
				<label htmlFor={this.props.id}>{this.props.label}</label>
				<input
					id={this.props.id}
					type={this.props.type}
					name={this.props.name}
					value={this.props.value}
					onChange={this.props.onChange}
					placeholder={this.props.placeholder}
				/>
				<span>{this.state.erroMsg}</span>
			</div>
		);
	}
}
