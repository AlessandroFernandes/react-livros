import React, { Component } from 'react';
import $ from 'jquery';
import InputCustomizado from '../common/NovoInput';
import axios from 'axios';
import PubSub from 'pubsub-js';
import Erros from '../common/Erros';

class CadastrarLivros extends Component {
	constructor(props) {
		super(props);
		this.state = { titulo: '', preco: '', autorId: '' };
		this.setPreco = this.setPreco.bind(this);
		this.setTitulo = this.setTitulo.bind(this);
		this.setId = this.setId.bind(this);
	}

	enviarForm(event) {
		event.preventDefault();

		let titulo = this.state.titulo.trim();
		let preco = this.state.preco.trim();
		let autorId = this.state.autorId;

		$.ajax({
			url: `https://cdc-react.herokuapp.com/api/livros`,
			type: 'POST',
			dataType: 'json',
			crossDomain: true,
			ContentType: 'application/JSON',
			data: JSON.stringify({
				titulo: titulo,
				preco: preco,
				autoId: autorId,
			}),
			success: function(resposta) {
				PubSub.publish('livros-publicados', resposta);
				this.setState({ titulo: '', preco: '', autoId: '' });
			},
			error: function(erro) {
				if (erro.status === 400) {
					new Erros().publicaErros(erro.responseJSON);
				}
			},
			beforeSend: () => PubSub.publish('limpar-erros', {}),
		});
		this.setState({ titulo: '', preco: '', autoId: '' });
	}

	setTitulo(event) {
		this.setState({ titulo: event.target.value });
	}

	setPreco(event) {
		this.setState({ preco: event.target.value });
	}
	setId(event) {
		this.setState({ autorId: event.target.value });
	}

	render() {
		const autor = this.props.autores.map(function(autor) {
			return (
				<option key={autor.id} value={autor.id}>
					{autor.nome}
				</option>
			);
		});
		// const autor = this.props.autores.map(autor => {
		// 	<option key={autor.id} value={autor.id}>
		// 		{autor.nome}
		// 	</option>
		// });
		return (
			<div>
				<form
					className="pure-form pure-form-aligned"
					onSubmit={this.enviarForm.bind(this)}
					method="post"
				>
					<InputCustomizado
						id="livro"
						type="text"
						name="livro"
						value={this.state.titulo}
						onChange={this.setTitulo}
						label="Titulo"
						placeholder="Livro"
					/>
					<InputCustomizado
						id="preco"
						type="number"
						name="preco"
						value={this.state.preco}
						onChange={this.setPreco}
						label="Preco"
						placeholder="Preço"
					/>

					<div className="pure-controls">
						<select
							value={this.state.autorId}
							name="autoId"
							onChange={this.setId}
						>
							<option value="">Selecione</option>
							{autor}
						</select>
					</div>
					<div className="pure-control-group">
						<label />
						<button
							type="submit"
							className="pure-button pure-button-primary"
						>
							Gravar
						</button>
					</div>
				</form>
			</div>
		);
	}
}

class TabelaLivros extends Component {
	render() {
		return (
			<div>
				<table className="pure-table">
					<thead>
						<tr>
							<th>Livro</th>
							<th>Preço</th>
							<th>Autor</th>
						</tr>
					</thead>
					<tbody>
						{this.props.livros.map(livro => (
							<tr key={livro.id}>
								<td>{livro.titulo}</td>
								<td>{livro.preco}</td>
								<td>{livro.autor.nome}</td>
							</tr>
						))}
					</tbody>
				</table>
			</div>
		);
	}
}

export default class LivrosBox extends Component {
	constructor(props) {
		super(props);
		this.state = { livros: [], autores: [] };
	}

	componentDidMount() {
		axios
			.get(`https://cdc-react.herokuapp.com/api/livros`)
			.then(resposta => this.setState({ livros: resposta.data }))
			.catch(erro => console.log(erro));

		axios
			.get(`https://cdc-react.herokuapp.com/api/autores`)
			.then(resposta => this.setState({ autores: resposta.data }))
			.catch(erro => console.log(erro));

		PubSub.subscribe('livros-publicados', (index, lista) =>
			this.setState({ livros: lista }),
		);
	}

	render() {
		return (
			<div>
				<div className="header">
					<h1>Cadastro de Livros</h1>
				</div>
				<div className="content" id="content">
					<CadastrarLivros autores={this.state.autores} />
					<TabelaLivros livros={this.state.livros} />
				</div>
			</div>
		);
	}
}
