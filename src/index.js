import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import AutoresBox from './components/autores/autores.Box';
import Home from './components/home/Home';
import LivrosBox from './components/livros/livros.box';

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

ReactDOM.render(
	<Router>
		<App>
			<Switch>
				<Route path="/" exact component={Home} />
				<Route path="/autores" component={AutoresBox} />
				<Route path="/livros" component={LivrosBox} />
				<Route path="*" component={Home} />
			</Switch>
		</App>
	</Router>,
	document.getElementById('root'),
);
